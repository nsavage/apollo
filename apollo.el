;; apollo.el --- notebook organizer for plain text notes

;; Copyright (C) 2020 Nicholas Savage <nick@nicksavage.ca>

;; Version: 0.1
;; Author: Nicholas Savage <nick.nicksavage.ca>
;; Keywords: plain text, notes, onenote
;; URL: https://gitlab.com/nsavage/apollo
;; Package-Requires: ((emacs "24.1"))

;; Apollo is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be sueful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public LIcense
;; along with this program. If not, see <https://www.gnu.org/licenses/>.

;; Code originally included in Deft is licensed under the "modified BSD license"
;; and was originally licensed under these original terms:

;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are met:
;; 1. Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;; 2. Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation  and/or other materials provided with the distribution.
;; 3. Neither the names of the copyright holders nor the names of any
;;    contributors may be used to endorse or promote products derived from
;;    this software without specific prior written permission.

;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
;; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;; POSSIBILITY OF SUCH DAMAGE.

;; This file is not part of GNU Emacs.

;; Commentary:

;; See README.md bundled with the file.

;; Code:

(require 'cl-lib)

(defgroup apollo nil
  "Emacs Apollo mode."
  :group 'local)

(defcustom apollo-directory "~/.notes"
  "Root directory of the Apollo directory."
  :group 'apollo
  :type 'directory
  :safe 'stringp)

(defcustom apollo-time-format " %Y-%m-%d %H:%M"
  "Format string for modification times in the Apollo browser.
Set to nil to hide."
  :type '(choice (string :tag "Time format")
                 (const :tag "Hide" nil))
  :group 'apollo)

(defcustom apollo-max-show-files 10
  "Limit Apollo to displaying x files.  Set to -1 for max."
  :group 'apollo
  :type 'integer)

(defcustom apollo-reset-context-on-start nil
  "Reset context upon loading Apollo or not"
  :group 'apollo
  :type 'boolean)

(defcustom apollo-default-context 0
  "Default apollo context. Takes an integer based on index of context in directory"
  :group 'apollo
  :type 'integer)

(defcustom apollo-strip-title-regexp
  (concat "\\(?:"
          "^%+" ; line beg with %
          "\\|^#\\+TITLE: *" ; org-mode title
          "\\|^[#* ]+" ; line beg with #, * and/or space
          "\\|-\\*-[[:alpha:]]+-\\*-" ; -*- .. -*- lines
          "\\|^Title:[\t ]*" ; MultiMarkdown metadata
          "\\|#+" ; line with just # chars
          "$\\)")
  "Regular expression to remove from file titles.
Presently, it removes leading LaTeX comment delimiters, leading
and trailing hash marks from Markdown ATX headings, leading
astersisks from Org Mode headings, and Emacs mode lines of the
form -*-mode-*-.
Copied from deft version 0.8"
  :type 'regexp
  :safe 'stringp
  :group 'apollo)

(defcustom apollo-use-root-directory nil
  "Option to include root directory in active contexts."
  :group 'apollo
  :type 'boolean)

;; Constants

(defconst apollo-version "0.1")
(defconst apollo-buffer "*Apollo*")

;; Global Variables
(defvar apollo-mode-hook nil
  "Hook run when entering Apollo mode.")

(defvar apollo-filter-hook nil
  "Hook run when the Apollo filter is run.")

(defvar apollo-open-file-hook nil
  "Hook run when Apollo opens a file.")

(defvar apollo-filter-term nil
  "Term to filter files by.  If nil, skip filtering.")

(defvar apollo-active-context 0
  "Integer to represent the active context.")

(defvar apollo-directories nil
  "List of subdirectories of `apollo-directory', used to represent contexts.")

(defvar apollo-mode-map
  (let ((map (make-keymap)))
    (define-key map (kbd "C-c C-q") 'apollo-close)
    (define-key map (kbd "C-c C-s") 'apollo-get-string-search)
    (define-key map (kbd "C-c C-g") 'apollo-buffer-refresh)
    (define-key map (kbd "C-c m") 'apollo-increment-max-files)
    (define-key map (kbd "C-c n") 'apollo-decrement-max-files)
    (define-key map (kbd "C-c C-r") 'apollo-increment-active-context)
    (define-key map (kbd "C-c C-e") 'apollo-decrement-active-context)
    map))

(defgroup apollo-faces nil
  "Faces used in Apollo mode"
  :group 'apollo
  :group 'faces)

(defface apollo-active-context-face
  '((t :inherit font-lock-keyword-face :bold t))
  "Face for Apollo active context."
  :group 'apollo-faces)

(defface apollo-context-face
  '((t :inherit font-lock-string-face))
  "Face for Apollo non-active contexts."
  :group 'apollo-faces)

(defface apollo-filename-face
  '((t :inherit font-lock-function-name-face :bold t))
  "Face for Apollo file titles."
  :group 'apollo-faces)

(defface apollo-search-face
  '((t :inherit font-lock-function-name-face :bold t))
  "Face for Apollo search string."
  :group 'apollo-faces)

(defface apollo-date-face
  '((t :inherit font-lock-variable-name-face))
  "Face for Apollo last modified times."
  :group 'apollo-faces)

;; Util functions

(defun apollo-set-mode-name ()
  "Set the mode line text."
  (setq mode-name "Apollo"))

(defun apollo-parse-contents (file)
  "Return the contents of FILE in order to parse title."
  (with-current-buffer (get-buffer-create "*Apollo temp*")
    (insert-file-contents file nil nil nil t)
    (concat (buffer-string))))

(defun apollo-parse-title (file)
  "Parse FILE and determine the title to display."
  (let* ((contents (apollo-parse-contents file))
	 (begin (string-match "^.+$" contents)))
    (if begin
	(apollo-strip-title (substring contents begin (match-end 0))))))

(defun apollo-chomp (str)
  "Trim leading and trailing whitespace from STR.
Copied from deft 0.8."
  (replace-regexp-in-string "\\(^[[:space:]\n]*\\|[[:space:]\n]*$\\)" "" str))

(defun apollo-strip-title (title)
  "Remove all strings matching `deft-strip-title-regexp' from TITLE.
Copied from deft 0.8."
  (apollo-chomp (replace-regexp-in-string apollo-strip-title-regexp "" title)))

(defun apollo-get-string-search ()
  "Gets a search string from user and call `apollo-buffer-setup' to filter the files in the context.  Run `apollo-filter-hook' once the string has been entered."
  (interactive)
  (let ((string (read-string "Search: ")))
    (if (eq 0 (length string))
	(setq apollo-filter-term nil)
      (setq apollo-filter-term string))
    (apollo-buffer-setup))
  (run-hooks 'apollo-filter-hook))

(defun apollo-set-initial-context ()
  "Decide whether to reset initial context of Apollo or not"
  (if apollo-reset-context-on-start
      (setq apollo-active-context apollo-default-context))
  
  ; if apollo-active-context hasn't been set before or got screwed up, reset
  (if (eq nil apollo-active-context)
      (setq apollo-active-context apollo-default-context)))

(defun apollo-get-context-list ()
  "Parse `apollo-directory' and build a list of all subdirectories to use as list of contexts."
  (let ((files (apollo-get-all-files apollo-directory))
	(results '()))

    (when apollo-use-root-directory
      (add-to-list 'results apollo-directory))
    (while (> (cl-list-length files) 0)
      (let ((file (pop files)))
	(if (file-directory-p file)
	    (add-to-list 'results file))))
    (setq apollo-directories (sort results (lambda (f1 f2) (apollo-file-name-less-p f1 f2))))))

(defun apollo-buffer-refresh ()
  "Call `apollo-buffer-setup', interactive version."
  (interactive)
  (apollo-buffer-setup))

(defun apollo-close ()
  "Quit Apollo."
  (interactive)
  (quit-window))

(defun apollo-set-active-context (x)
  "Set the active context to X."
  (setq apollo-active-context x)
  (apollo-filter-initialize)
  (apollo-buffer-setup))

(defun apollo-increment-active-context ()
  "Switch active context to next context."
  (interactive)
  (if (< (+ apollo-active-context 1) (cl-list-length apollo-directories))
      (apollo-set-active-context (+ apollo-active-context 1))
    (apollo-set-active-context 0)))

(defun apollo-decrement-active-context ()
  "Switch active context to previous context."
  (interactive)
  (if (> apollo-active-context 0)
      (apollo-set-active-context (- apollo-active-context 1))
    (apollo-set-active-context (- (cl-list-length apollo-directories) 1))))

(defun apollo-file-mtime (file)
  "Return the mtime of FILE."
  (nth 5 (file-attributes (file-truename file))))

(defun apollo-file-newer-p (file1 file2)
  "Return non-nil if FILE1 was modified since FILE2 and nil otherwise."
  (let (time1 time2)
    (setq time1 (apollo-file-mtime file1))
    (setq time2 (apollo-file-mtime file2))
    (time-less-p time2 time1)))

(defun apollo-sort-files (files)
  "Sort FILES based in alphabetical order."
  (sort files (lambda (f1 f2) (apollo-file-newer-p f1 f2))))

(defun apollo-file-name-less-p (file1 file2)
  "Return non-nil if FILE1 is alphabetically less than FILE2."
  (let ((t1 (file-name-nondirectory file1))
	(t2 (file-name-nondirectory file2)))
    (string-lessp (and t1 (downcase t1))
		  (and t2 (downcase t2)))))

(defun apollo-display-tab-row ()
  "Output the list of contexts as the top tab row."
  (let ((tab-count -1)
	(total-width (window-text-width)))
    
    (while (< tab-count (cl-list-length apollo-directories))
      (setq tab-count (+ tab-count 1))
      (let ((directory (nth tab-count apollo-directories)))
	(if (not (eq nil directory))
	    (progn
	      (let ((name (file-name-nondirectory directory)))
		(apollo-define-tab name tab-count))))))
    (widget-insert "\n")))
  
(defun apollo-define-tab (name tab-count)
  "Create a link widget for each individual context tab with NAME.
Widget returns `TAB-COUNT' to set the active context."
  (let ((temp-face 'apollo-context-face))
    (when (eq tab-count apollo-active-context)
      (setq temp-face 'apollo-active-context-face))
  
      (widget-create 'link
		     :button-face temp-face
		     :format "%[%v%]"
		     :tag tab-count
		     :notify (lambda (widget &rest ignore)
			       (apollo-set-active-context (widget-get widget :tag)))
		 name)))

(defun apollo-get-tab-names ()
  "Get the names of the current contexts to display on buffer."
  (let ((list apollo-directories)
	(return-list '())
	(counter apollo-active-context))
	
    (while (> (cl-list-length list) 0)
      (let ((filename (pop list)))
	(add-to-list 'return-list (file-name-nondirectory filename))))
    return-list))

(defun apollo-open-file (file)
 "Open FILE in a new buffer."
  (let ((buffer (find-file-noselect file))) ;
    (switch-to-buffer buffer)
    (run-hooks 'apollo-open-file-hooks)))

;; Filtering

(defun apollo-filter-initialize ()
  "Initialize the filter string (nil) and file list (all files)."

  (setq apollo-filter-term nil))

(defun apollo-filter-match-file (file)
  "Test if FILE matches the current filter."
  (with-current-buffer (get-buffer-create "temp-buffer")
    (erase-buffer)
  ;; (with-temp-buffer
    ;; (insert file)
    (let ((title (apollo-parse-title file)))
      (when title
	(insert title))
      (insert-file-contents file nil nil nil t))
    (goto-char (point-min))
    (if (re-search-forward apollo-filter-term nil t)
	file)))

(defun apollo-current-window-width ()
  "Return current width of window displaying `apollo-buffer'.
If the frame has a fringe, it will absorb the newline.
Otherwise, we reduce the line length by a one-character offset."
  ;; (- (window-text-width) 1))
  (let* ((window (get-buffer-window apollo-buffer))
         (fringe-right (ceiling (or (cadr (window-fringes)) 0)))
         (offset (if (> fringe-right 0) 0 1)))
    (when window
      (- (window-text-width window) offset))))


(defun apollo-file-widget (filename)
  "Takes FILENAME and outputs it on the buffer.  It is responsible for displaying the filename as `widget-create', which is truncated if too long, then add spaces until it displays the date.  Date format is customizable with `apollo-date-face'."
  
  (let* (
	(mtime (format-time-string apollo-time-format (apollo-file-mtime filename)))
	(mtime-width (string-width mtime))
	(line-width (- (apollo-current-window-width) mtime-width))
	(title (apollo-parse-title filename))
	(title-width (if (> (length title) line-width)
			 (- line-width 5)
		       (length title)))
	(title (substring title 0 title-width)))

    (widget-create 'link
		   :button-face 'apollo-filename-face
                   :format "%[%v%]" ; tells emacs to display the value as the link name
		   :tag filename ;passed to apollo-open-file when its clicked
		   :notify (lambda (widget &rest ignore)
			     (apollo-open-file (widget-get widget :tag)))
		   title)
    
    (while (< (current-column) line-width)
      (widget-insert " "))
    (widget-insert (propertize mtime 'face 'apollo-date-face))
  (widget-insert "\n")))

(defun apollo-get-all-files (dir)
  "Gets all files in context, without being filtered.
The so-called non-filtered list excludes hidden files (that start with a .)
and files that start with #.
Argument DIR directory that will be displayed as a context."
  (let ((files (directory-files dir t "^[^\.\#]")))
    (apollo-sort-files files)))

(defun apollo-get-filtered-files (dir)
  "Gets the list of files in the context and applies `apollo-filter-term'
against them.
Argument DIR Context directory."
  (let ((files (apollo-get-all-files dir))
	(results '()))
    (if (eq nil apollo-filter-term)
	files
      (while (> (cl-list-length files) 0)
	(let ((file (pop files)))
	  (if (not (file-directory-p file))
	      (if (not (eq nil (apollo-filter-match-file file)))
		  (add-to-list 'results file)))))
      results)))
    
(defun apollo-display-files ()
  "Outputs filtered files in a context."
  (let* ((counter 0)
	 (active-directory (nth apollo-active-context apollo-directories))
	 (files (apollo-get-filtered-files active-directory))
	 (max-show (if (eq -1 apollo-max-show-files) (cl-list-length files) apollo-max-show-files)))
    (setq counter 0)
    (while (and (not (eq nil files)) (< counter apollo-max-show-files))
      (let ((filename (pop files)))
	(if (not (file-directory-p filename))
	    (apollo-file-widget filename)))
      (setq counter (+ counter 1)))))

(defun apollo-display-search-row ()
  "Decides what to display on the second row.  If there is an `apollo-filter-term' display that, else display nothing."
  (if (eq nil apollo-filter-term)
      (widget-insert "\n")
    (widget-insert (propertize (format "Search: %s\n" apollo-filter-term) 'face 'apollo-search-face))))

(defun apollo-buffer-setup ()
  "Output the current display in the Apollo buffer."
  (let ((inhibit-read-only t))
    (erase-buffer))
  ;; header
  (apollo-display-tab-row)
  (apollo-display-search-row)
  (apollo-display-files)
  (apollo-set-default-directory)
  (goto-char (point-min)))

(defun apollo-set-default-directory ()
  "Set the Emacs `default-directory' to the directory of the active context."
  (setq default-directory (nth apollo-active-context apollo-directories)))

(defun apollo-increment-max-files ()
  "Adds 10 to `apollo-max-show-files' and refreshes the buffer"
  (interactive)
  (setq apollo-max-show-files (+ apollo-max-show-files 10))
  (apollo-buffer-setup))

(defun apollo-decrement-max-files ()
  "Subtracts 10 to `apollo-max-show-files' and refreshes the buffer"
  (interactive)
  (setq apollo-max-show-files (- apollo-max-show-files 10))
  (apollo-buffer-setup))

(defun apollo-set-max-show-files ()
  "Sets `apollo-max-show-files' to number received from user."
  (interactive)
  (let ((number (string-to-number(read-string "Show how many files? (-1 for all)"))))
    (apollo-buffer-setup)
    (setq apollo-max-show-files number))
  (apollo-buffer-setup))

(defun apollo-mode ()
  "Major mode for browsing, filtering and editing plain text notes. Turning on `apollo-mode' runs the hook `apollo-mode-hook'."
  (setq major-mode 'apollo-mode)
  (setq buffer-read-only t)
  (setq truncate-lines t)
  
  (kill-all-local-variables)
  (apollo-get-context-list)
  (apollo-set-mode-name)
  (apollo-filter-initialize)
  (apollo-set-initial-context)
  (apollo-buffer-setup)
  (use-local-map apollo-mode-map)
  (widget-setup)
  (run-mode-hooks 'apollo-mode-hook))

(put 'apollo-mode 'mode-class 'special)

(defun apollo ()
  "Switch to Apollo buffer and load files."
  (interactive)
  (switch-to-buffer apollo-buffer)
  (apollo-mode))

(provide 'apollo)

;; TODO:
;; Listing includes autosaves/file locks that show double the files if the file is changed and not saved.
;; Show descriptions
;; dates overflow in putty by 1

;;; apollo.el ends here
