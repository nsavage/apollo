# Apollo for Emacs

Apollo is an Emacs mode for quickly browsing, filtering, and editing
directories of plain text notes, inspired by very heavily by
Deft.  It was designed for increased productivity when writing and
taking notes by making it fast and simple to find the right file at
the right time.  It departs from Deft in that it implements contexts in order
to provide the relevant files.

Obtaining Apollo
-----------------
Apollo is open source software and may be freely distributed and modified
under the BSD license.  It is currently in development, so there are no stable
releases available as of yet.

To follow or contribute to Apollo develpoment, you can browse or clone the
Git reponsitory [on Gitlab](https://gitlab.com/nsavage/apollo):

    git clone https://gitlab.com/nsavage/apollo.git

Overview
---------

The Apollo buffer is a file browser that does two things: It lists all
subdirectories of the Apollo folder at the top, and lists all files within
those subdirectories below.  The user activates a folder (a tab as they are
referred to below).

By default, the contexts used are the subdirectories of `apollo-directory`, skipping the root
directory itself (although this can be customized).

For each context, once activated, Apollo will search for all files within that subdirectory
that meet `apollo-filter-term`. If `apollo-filter-term` is nil, it will search for all files. 
The filter term is set by `apollo-get-string-search`, by default <kbd>C-C C-s</kbd>, which requests
a string from the minibuffer. If the given string is empty, it resets to nil and shows all files.

Basic Customization
-------------------

You can customize items in the `apollo` group to change default functionality.

By default, Apollo looks for notes in the `~/.deft` directory, but this can be customized by
setting the variable to another directory.


History
-------

Apollo originally began as a fork of Deft, but grew enough in another direction that it
stands alone. A number of functions were copied wholesale from Deft, at least in version
0.1, such as `apollo-strip-title-regexp` (which started life as `deft-strip-title-regexp`.
In theory these could have called the Deft functions instead, but that would create a
dependency with Deft that was not considered necessary.
